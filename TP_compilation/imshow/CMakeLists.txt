cmake_minimum_required( VERSION 3.0 )
project( sayHello )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package(PkgConfig REQUIRED)
pkg_check_modules( PKGS REQUIRED opencv )
include_directories( ${PKGS_INCLUDE_DIRS} )

add_executable(imshow imshow.cpp)

target_link_libraries( imshow ${PKGS_LIBRARIES} )
